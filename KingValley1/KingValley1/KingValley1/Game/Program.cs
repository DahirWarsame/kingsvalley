using System;

namespace KingValley1
{
#if WINDOWS || XBOX
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        static void Main(string[] args)
        {
            using (Kingsvalley game = new Kingsvalley())
            {
                game.Run();
            }
        }
    }
#endif
}

